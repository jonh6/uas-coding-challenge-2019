
// Created by Jonathan Hirsch on 9/11/19.
// reads coordinates, stores in vector and provides method for updating location
// and retrieving data
//

#include "GPS.h"

static const char* WAYPOINTS_FILE_NAME = "missionwaypoints.txt";

GPS::GPS() { // get data, in this case from file, store in vector "waypoints"
    ifstream fin(WAYPOINTS_FILE_NAME, ios::in);
    double lon, lat;

    string empty;
    while (!fin.eof()) {
        fin >> lat >> lon;
        waypoints.push_back(Point(lat, lon));
        getline(fin, empty);
    }

    fin.close();
    getFirstLocation();
}

bool GPS::hasNextLocation(){ // checks if there are new locations
    return !(listCtr == waypoints.size());
}
  Point GPS::getNextElement() { // return next element in vector, increments counter
    return waypoints.at(listCtr++);
}

void GPS::getFirstLocation() { // assign curr_loc to first element in list
    curr_loc = getNextElement();
}

bool GPS::updateLocation() { // checks if there is new location data, stores previous location and updates current location
    if (hasNextLocation()) { // returns true if data is available and data update succeeds
        prev_loc = curr_loc;
        curr_loc = getNextElement();
        return true;
    }
    return false;
}

Point GPS::getPreviousLocation(){ // return previous location
    return prev_loc;
}

Point GPS::getCurrentLocation(){ // return current location
    return curr_loc;
}