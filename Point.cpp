//
// Created by Jonathan Hirsch on 9/12/19.
// point vector for basic operation for GPS coordinates
//

#include "Point.h"

Point::Point(double lat, double lon){ // constructor
    this -> lat = lat;
    this -> lon = lon;
}

ostream& operator <<(ostream &os, const Point p) { // overload << operator for output
    os << p.lat;

    if (p.lat > 0){os << "N ";}
    else{os << "S ";}

    os << p.lon;

    if (p.lon > 0){os << "E";}
    else{os << "W";}
    return os;
}

Point operator -(const Point& p1, const Point& p2){ // overload - operator for difference between two points
    return Point (p1.lat - p2.lat, p1.lon - p2.lon);
}


Point Point::toRadians(){ // converts point to radian
    return Point(toRadians(lat), toRadians(lon));
}

double Point::toRadians(double angle){ // converts degrees to radian
    return angle * M_PI / 180;
}

double Point::getLat() { // return lattitude
    return lat;
}

double Point::getLong() { // return longitude
    return lon;
}