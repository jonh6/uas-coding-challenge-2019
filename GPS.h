//
// Created by Jonathan Hirsch on 9/11/19.
//
#pragma once
#ifndef GPS_SPEED_CHALLENGE_GPS_H
#define GPS_SPEED_CHALLENGE_GPS_H

#include <string>
#include <fstream>
#include <vector>
#include "Point.h"

using namespace std;

class GPS{
public:
    GPS();
    bool hasNextLocation();
    bool updateLocation();
    Point getPreviousLocation();
    Point getCurrentLocation();


private:
    vector<Point> waypoints;
     int listCtr = 0;
     Point curr_loc;
     Point prev_loc;
    Point getNextElement();
    void getFirstLocation();
};

#endif //GPS_SPEED_CHALLENGE_GPS_H


