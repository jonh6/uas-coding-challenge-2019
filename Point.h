//
// Created by Jonathan Hirsch on 9/12/19.
//

#ifndef GPS_SPEED_CHALLENGE_POINT_H
#define GPS_SPEED_CHALLENGE_POINT_H

#include <math.h>
#include <fstream>

using namespace std;
class Point{
    friend ostream& operator << (ostream& os, const Point p);
    friend Point operator -(const Point& p1, const Point& p2);
public:
    Point(){;};
    Point (double lat, double lon);
    Point toRadians();
    double getLat();
    double getLong();

private:
    double lat;
    double lon;
    double toRadians(double num);
};
#endif //GPS_SPEED_CHALLENGE_POINT_H
