//
// Created by Jonathan Hirsch on 9/11/19.
//
#include "GPS.h"
#include "Point.h"
#include <iomanip>
#include <iostream>
#include <zconf.h>

static const int UPDATE_TIME = 1; // update time in seconds
static const int EARTH_RADIUS = 6378100; // average earth radius in m


long double distanceMoved(Point prev_loc, Point curr_loc){ // Haversine formula for distance across a sphere. With rounding, accurate to a few meters
                                                        // due to 15 max decimals on double (up to 19 w/ long double)

    /* Haversine formula:	a = sin²(Δφ/2) + cos φ1 ⋅ cos φ2 ⋅ sin²(Δλ/2)
                            c = 2 ⋅ atan2( √a, √(1−a) )
                            d = R ⋅ c
                            where φ is latitude, λ is longitude, R is earth average radius */
   long double prev_loc_lat = prev_loc.toRadians().getLat();
   long double curr_loc_lat = curr_loc.toRadians().getLat();

   Point displacement = (prev_loc - curr_loc).toRadians();

    long double a = pow(sin(displacement.getLat()/2), 2)
            + cos(prev_loc_lat) * cos(curr_loc_lat)
            * pow (sin(displacement.getLong()/2), 2);

   long double c = 2* atan2(sqrt(a),sqrt(1-a));

    return EARTH_RADIUS * c; // d = R ⋅ c
}

int main()
{
    GPS gps;
    Point prev_loc;
    Point curr_loc;

    long double distance;
    double total_distance = 0;
    double average_speed;
    int time; // in seconds, considered as 1 loop = 1 second for simplicity

    for (time = 1; gps.updateLocation(); time ++){ // update gps location

        curr_loc = gps.getCurrentLocation();
        prev_loc = gps.getPreviousLocation(); // update current & previous location

        distance = distanceMoved(prev_loc, curr_loc); // calculate distance
        total_distance += distance;
        average_speed = total_distance / time; // compute total distance and average speed

    cout << setprecision(16) << "Location: " << curr_loc
         << setprecision(2) << " Average Speed: "
         << average_speed << "m/s" << endl << endl;

    sleep(UPDATE_TIME); // sleep for given time
    }
return 0;
}//